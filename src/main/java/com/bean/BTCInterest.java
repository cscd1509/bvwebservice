package com.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class BTCInterest implements Serializable{
 
    private BigDecimal btcnow;
    private BigDecimal btcup;

    public BigDecimal getBtcnow() {
        return btcnow;
    }

    public void setBtcnow(BigDecimal btcnow) {
        this.btcnow = btcnow;
    }

    public BigDecimal getBtcup() {
        return btcup;
    }

    public void setBtcup(BigDecimal btcup) {
        this.btcup = btcup;
    }

    public BTCInterest(BigDecimal btcnow, BigDecimal btcup) {
        this.btcnow = btcnow;
        this.btcup = btcup;
    }

    public BTCInterest() {
    }
    
}
