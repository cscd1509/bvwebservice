package com.bv.beans.responses;

import java.io.Serializable;
import java.util.Date;

public class ResponseAuth implements Serializable {

    private String username;
    private String token;
    private Date expireDate;

    public ResponseAuth(String username, String token, Date expireDate) {
        this.username = username;
        this.token = token;
        this.expireDate = expireDate;
    }

    public ResponseAuth() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

}
