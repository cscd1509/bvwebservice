/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bv.controllers;

import com.bv.beans.responses.ResponseApi;
import com.bv.entities.Admin;
import com.bv.facades.AdminFacade;
import com.bv.facades.HistoryDepositFacade;
import com.bv.facades.SystemFundFacade;
import com.bv.utils.LogUtils;
import java.math.BigDecimal;
import java.util.List;
import javafx.util.Pair;
import javax.servlet.http.HttpSession;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pc3-cellx
 */
@RestController
@RequestMapping(value = "/Api/Admin")
public class AdminController {

    @PostMapping(value = "/Deposit")
    public ResponseApi CreateDeposit(@RequestParam(value = "adminUsername") String adminUsername,
            @RequestParam(value = "adminPassword") String adminPassword,
            @RequestParam(value = "receivingCustomerUsername") String receivingCustomerUsername,
            @RequestParam(value = "amount") BigDecimal amount, @RequestParam(value = "discountPercent", required = false, defaultValue = "0") BigDecimal discountPercent,
            ModelMap mm, HttpSession session) {
        ResponseApi res = new ResponseApi();
        try {
            Admin admin = new AdminFacade().checkLogin(adminUsername, adminPassword);
            if (admin == null) {
                res.setCode(2);
                res.setMessage("Tên đăng nhập hoặc mật khẩu không chính xác!");
            } else if (!admin.getIsActive()) {
                res.setCode(3);
                res.setMessage("Tài khoản quản trị đã bị khóa!");
            } else {
                Pair<Integer, String> result = new HistoryDepositFacade().deposit(admin.getId(), receivingCustomerUsername, amount, discountPercent);
                res.setCode(result.getKey());
                res.setMessage(result.getValue());
                if (result.getKey() == 1) {
                    LogUtils.logs(admin.getId(), LogUtils.ACTION_ADD, 9, "Nạp tiền thành công cho " + receivingCustomerUsername + " , " + amount + "đ");
                    res.setSuccess(true);
                } else {
                    LogUtils.logs(admin.getId(), LogUtils.ACTION_ADD, 9, "Nạp tiền thất bại cho " + receivingCustomerUsername + " , " + amount + "đ");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @GetMapping(value = "/CompanyFund")
    public ResponseApi CompanyFund() {
        ResponseApi responseApi = new ResponseApi();
        try {
            List items = new SystemFundFacade().showCompany();
            responseApi = ResponseApi.createSuccessResponse(items);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseApi;
    }
}
