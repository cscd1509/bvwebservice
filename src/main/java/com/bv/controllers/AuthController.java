/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bv.controllers;

import com.bv.beans.responses.ResponseApi;
import com.bv.facades.CustomerFacade;
import javafx.util.Pair;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pc3-cellx
 */
@RestController
@RequestMapping(value = "/Api/Auth")
public class AuthController {

    @PostMapping(value = "/Register")
    public ResponseApi Register(@RequestParam("userName") String userName, @RequestParam("fullName") String fullName,
            @RequestParam(value = "refID") String refID,
            @RequestParam("password") String password, @RequestParam("email") String email,
            @RequestParam(value = "mobile", required = false) String mobile, @RequestParam(value = "peoplesIdentity", required = false) String peoplesIdentity,
            @RequestParam(value = "bankName", required = false) String bankName,
            @RequestParam(value = "bankNumber", required = false) String bankNumber,
            @RequestParam(value = "bankAgency", required = false) String bankAgency,
            @RequestParam(value = "gender", required = false) Boolean gender, HttpServletRequest request) {
        ResponseApi res = new ResponseApi();
        try {
            Pair<Integer, String> result = new CustomerFacade().createUser(
                    fullName, password, email, refID, mobile, gender, userName, peoplesIdentity, bankName, bankNumber, bankAgency
            );
            if (result.getKey() == 1) {
                res = ResponseApi.createSuccessResponse();
            }
            res.setMessage(result.getValue());
            res.setCode(result.getKey());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
