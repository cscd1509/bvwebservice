/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bv.controllers;

import com.bean.PagerPlus;
import com.bv.beans.responses.ResponseApi;
import com.bv.facades.CheckAwardFacade;
import com.bv.facades.HistoryAwardFacade;
import com.bv.facades.HistoryChangeRankFacade;
import com.bv.services.TokenAuthenticationService;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pc3-cellx
 */
@RestController
@RequestMapping(value = "/Api/Award")
public class AwardController {

    @GetMapping(value = "/ListAward")
    public ResponseApi HistoryAward(@RequestParam(value = "currentPage", required = false, defaultValue = "1") int currentPage,
            @RequestParam(value = "displayPerPage", required = false, defaultValue = "10") int displayPerPage,
            @RequestParam(value = "checkAwardID", required = false) Integer checkAwardID,
            @RequestParam(value = "isAvailability", required = false) Integer isAvailability,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date endDate, HttpServletRequest request) {
        ResponseApi responseApi = new ResponseApi();
        try {
            PagerPlus pager = new PagerPlus();
            pager.setCurrentPage(currentPage);
            pager.setDisplayPerPage(displayPerPage);
            pager.setAsc(false);
            pager.setOrderColumn("createdDate");
            pager.setIsAvailability(isAvailability);
            if (startDate != null && endDate != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(endDate);
                c.set(Calendar.HOUR_OF_DAY, 23);
                c.set(Calendar.MINUTE, 59);
                c.set(Calendar.SECOND, 59);
                c.set(Calendar.MILLISECOND, 9999);
                endDate = c.getTime();
                pager.setStartDate(startDate);
                pager.setEndDate(endDate);
            }
            pager.setIsCheck(checkAwardID);
            List items = new HistoryAwardFacade().pager(pager, TokenAuthenticationService.getUserName(request));
            HashMap paging = new HashMap();
            paging.put("pager", pager);
            paging.put("items", items);
            responseApi = ResponseApi.createSuccessResponse(paging);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseApi;
    }

    @GetMapping(value = "/ListCheckAward")
    public ResponseApi ListCheckAward(HttpServletRequest request) {
        ResponseApi responseApi = new ResponseApi();
        try {
            List items = new CheckAwardFacade().getAvailableCheckAward();
            responseApi = ResponseApi.createSuccessResponse(items);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseApi;
    }

    @GetMapping(value = "/HistoryChangeRank")
    public ResponseApi HistoryAward(HttpServletRequest request) {
        ResponseApi responseApi = new ResponseApi();
        try {
            List items = new HistoryChangeRankFacade().pager(TokenAuthenticationService.getUserName(request));
            responseApi = ResponseApi.createSuccessResponse(items);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseApi;
    }
}
