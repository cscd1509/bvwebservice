/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bv.controllers;

import com.bv.beans.responses.ResponseApi;
import com.bv.facades.CustomerFacade;
import com.bv.services.TokenAuthenticationService;
import com.bv.utils.StringUtils;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pc3-cellx
 */
@RestController
@RequestMapping(value = "/Api/Customer")
public class CustomerController {

    @GetMapping(value = "/Information")
    public ResponseApi Information(HttpServletRequest request) {
        ResponseApi res = new ResponseApi();
        try {
            String username = TokenAuthenticationService.getUserName(request);
            res = ResponseApi.createSuccessResponse(new CustomerFacade().getBasicInfomation(username));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @GetMapping(value = "/TreeParent")
    public ResponseApi TreeParent(@RequestParam(value = "firstUsername", required = false) String username,
            @RequestParam(value = "maxLevel", required = false, defaultValue = "999999") Integer maxLevel,
            HttpServletRequest request) {
        ResponseApi res = new ResponseApi();
        try {
            if (StringUtils.isEmpty(username)) {
                username = TokenAuthenticationService.getUserName(request);
            }
            res = ResponseApi.createSuccessResponse(new CustomerFacade().getTreeParent(username, maxLevel));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @GetMapping(value = "/TreeReferral")
    public ResponseApi TreeReferral(@RequestParam(value = "firstUsername", required = false) String username,
            @RequestParam(value = "maxLevel", required = false, defaultValue = "999999") Integer maxLevel,
            HttpServletRequest request) {
        ResponseApi res = new ResponseApi();
        try {
            if (StringUtils.isEmpty(username)) {
                username = TokenAuthenticationService.getUserName(request);
            }
            res = ResponseApi.createSuccessResponse(new CustomerFacade().getTreeReferral(username, maxLevel));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
