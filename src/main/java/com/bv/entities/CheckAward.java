package com.bv.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CheckAward")
public class CheckAward implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    private int id;

    @Column(name = "Name", insertable = false, updatable = false)
    private String name;

    @Column(name = "IsDeleted", insertable = false, updatable = false)
    private boolean isDeleted;

    @Column(name = "PercentAward", insertable = false, updatable = false)
    private BigDecimal percentAward;

    @Column(name = "IsDeleted", insertable = false, updatable = false)
    private BigDecimal numberAward;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public CheckAward(int id) {
        this.id = id;
    }

    public CheckAward() {
    }

    public BigDecimal getPercentAward() {
        return percentAward;
    }

    public void setPercentAward(BigDecimal percentAward) {
        this.percentAward = percentAward;
    }

    public BigDecimal getNumberAward() {
        return numberAward;
    }

    public void setNumberAward(BigDecimal numberAward) {
        this.numberAward = numberAward;
    }

}
