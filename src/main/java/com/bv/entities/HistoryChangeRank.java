package com.bv.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "HistoryChangerank")
public class HistoryChangeRank implements java.io.Serializable {

    private Integer id;
    private Customer customerID;
    private RankName rankID;
    private String rankName;
    private Date createdDate;

    public HistoryChangeRank() {
    }

    public HistoryChangeRank(int id) {
        this.id = id;
    }

    public HistoryChangeRank(Integer id,int rankID,String rankIDName,String rankIDColor, String rankName, Date createdDate) {
        this.id = id;
        this.rankID = new RankName(rankID, rankIDName, rankIDColor);
        this.rankName = rankName;
        this.createdDate = createdDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerID")
    public Customer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customer customerID) {
        this.customerID = customerID;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RankID")
    public RankName getRankID() {
        return rankID;
    }

    public void setRankID(RankName rankID) {
        this.rankID = rankID;
    }

    @Column(name = "RankName")
    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", insertable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
