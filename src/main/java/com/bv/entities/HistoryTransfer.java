package com.bv.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "HistoryTransfer")
public class HistoryTransfer implements java.io.Serializable {

    private Integer id;
    private Admin sendindAdminID;
    private Admin receivingAdminID;
    private BigDecimal amount;
    private BigDecimal amountBeforeSending;
    private BigDecimal amountBeforeReceving;
    private Date createdDate;

    public HistoryTransfer() {
    }

    public HistoryTransfer(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", insertable = false, updatable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sendindAdminID")
    public Admin getSendindAdminID() {
        return sendindAdminID;
    }

    public void setSendindAdminID(Admin sendindAdminID) {
        this.sendindAdminID = sendindAdminID;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReceivingAdminID")
    public Admin getReceivingAdminID() {
        return receivingAdminID;
    }

    public void setReceivingAdminID(Admin receivingAdminID) {
        this.receivingAdminID = receivingAdminID;
    }

    @Column(name = "Amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Column(name = "AmountBeforeSending")
    public BigDecimal getAmountBeforeSending() {
        return amountBeforeSending;
    }

    public void setAmountBeforeSending(BigDecimal amountBeforeSending) {
        this.amountBeforeSending = amountBeforeSending;
    }

    @Column(name = "AmountBeforeReceving")
    public BigDecimal getAmountBeforeReceving() {
        return amountBeforeReceving;
    }

    public void setAmountBeforeReceving(BigDecimal amountBeforeReceving) {
        this.amountBeforeReceving = amountBeforeReceving;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedDate", insertable = false, updatable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
    
}