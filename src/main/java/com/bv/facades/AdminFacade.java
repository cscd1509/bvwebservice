package com.bv.facades;

import com.bean.Pager;
import com.bv.entities.Admin;
import com.bv.entities.Agency;
import com.bv.entities.ModuleInRole;
import com.bv.entities.RoleAdmin;
import com.bv.utils.CustomFunction;
import com.bv.utils.StringUtils;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class AdminFacade extends AbstractFacade {

    public AdminFacade() {
        super(Admin.class);
    }

    public List pager(Pager pager) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.add(Restrictions.eq("isDelete", false));
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(Admin.class);
                cr.createAlias("roleAdmID", "roleAdmID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("roleAdmID", FetchMode.JOIN);
                cr.createAlias("agencyID", "agencyID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("agencyID", FetchMode.JOIN);
                cr.add(Restrictions.eq("isDelete", false));
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                list = cr.list();
            }
        } catch (Exception e) {
            Logger.getLogger(entityClass.getName()).log(Level.SEVERE, null, e);
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public Admin checkLogin(String username, String password) throws Exception {
        Session session = null;
        Admin result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(Admin.class);
            cr.add(Restrictions.eq("userName", username));
            cr.add(Restrictions.eq("password", password));
            cr.add(Restrictions.eq("isDelete", false));
            result = (Admin) cr.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }

    public void block(Integer id, Boolean status) throws Exception {
        Transaction trans = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                trans = session.beginTransaction();
                Query query = session.createSQLQuery("update admin set isActive=:isActive where id=:id").setParameter("isActive", status).setParameter("id", id);
                query.executeUpdate();
                trans.commit();
            }
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public Admin findAdminByUsername(String userName) throws Exception {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.add(Restrictions.eq("userName", userName));
                cr.add(Restrictions.eq("isDelete", false));
                return (Admin) cr.uniqueResult();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return null;
    }

    public Integer getAdminRoleByAdminId(int admId) {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.add(Restrictions.eq("id", admId));
                cr.setProjection(Projections.projectionList().add(Projections.property("roleAdmID.id")));
                return (Integer) cr.uniqueResult();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return null;
    }

    public Integer getAdminAgencyByAdminId(int admId) {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.add(Restrictions.eq("id", admId));
                cr.setProjection(Projections.projectionList().add(Projections.property("provincialAgencyID.id")));
                return (Integer) cr.uniqueResult();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return null;
    }

    public Admin findAdminById(Integer id) {
        Session session = null;
        Admin admin = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.createAlias("roleAdmID", "roleAdmID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("roleAdmID", FetchMode.JOIN);
                cr.add(Restrictions.eq("id", id));
                cr.add(Restrictions.eq("isDelete", false));
                admin = (Admin) cr.uniqueResult();
                if (admin != null) {
                    Hibernate.initialize(admin.getRoleAdmID().getModuleInRoles());
                    for (ModuleInRole mr : (admin.getRoleAdmID().getModuleInRoles())) {
                        Hibernate.initialize(mr.getModuleID());
                        Hibernate.initialize(mr.getModuleID().getModules());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return admin;
    }

    public void delete(int id) throws Exception {
        Transaction trans = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                trans = session.beginTransaction();
                Query query = session.createSQLQuery("update admin set isDelete=:isDelete where id=:id").setParameter("isDelete", true).setParameter("id", id);
                query.executeUpdate();
                trans.commit();
            }
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public int create(Map admin) throws Exception {
        Transaction trans = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            if (admin == null) {
                return 1;
            }
            if (StringUtils.isEmpty(admin.get("userName").toString())
                    || StringUtils.isEmpty(admin.get("name").toString())
                    || StringUtils.isEmpty(admin.get("password").toString())) {
                return 2;
            }
            Admin a = new Admin();
            a.setName(StringUtils.escapeHtmlEntity(admin.get("name").toString()));
            a.setPassword(CustomFunction.md5(admin.get("password").toString()));
            a.setUserName(StringUtils.escapeHtmlEntity(admin.get("userName").toString()));
            a.setMobile(StringUtils.escapeHtmlEntity(admin.get("mobile") == null ? null : admin.get("mobile").toString()));
            a.setEmail(StringUtils.escapeHtmlEntity(admin.get("email") == null ? null : admin.get("email").toString()));
            a.setAddress(StringUtils.escapeHtmlEntity(admin.get("address") == null ? null : admin.get("address").toString()));
            a.setTaxCode(StringUtils.escapeHtmlEntity(admin.get("taxCode") == null ? null : admin.get("taxCode").toString()));
            a.setBillingAddress(StringUtils.escapeHtmlEntity(admin.get("billingAddress") == null ? null : admin.get("billingAddress").toString()));
            a.setRoleAdmID(new RoleAdmin(Integer.parseInt(admin.get("roleAdmID").toString())));
            a.setAgencyID(new Agency(Integer.parseInt(admin.get("agencyID").toString())));
            a.setIsActive(true);
            if (findAdminByUsername(a.getUserName()) != null) {
                return 3;
            }
            session.save(a);
            trans.commit();
            return 4;
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

    public int changePassword(String oldpass, String newpass, int myId) throws Exception {
        Session session = null;
        Transaction trans = null;
        int rs = 0;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q;
            if (oldpass == null) {
                q = session.createSQLQuery("update Admin set Password=:newpass where ID=:id");
                q.setParameter("newpass", CustomFunction.md5(newpass))
                        .setParameter("id", myId);
            } else {
                q = session.createSQLQuery("update Admin set Password=:newpass where ID=:id and Password=:oldpass");
                q.setParameter("newpass", CustomFunction.md5(newpass))
                        .setParameter("oldpass", CustomFunction.md5(oldpass))
                        .setParameter("id", myId);
            }
            rs = q.executeUpdate();
            trans.commit();
        } catch (Exception e) {
            if (trans != null) {
                try {
                    trans.rollback();
                } catch (Exception ex) {
                    throw ex;
                }
            }
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }

    public Admin findAdminInfoByUsername(String userName) throws Exception {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(Admin.class);
                cr.createAlias("roleAdmID", "roleAdmID", JoinType.LEFT_OUTER_JOIN);
                cr.setFetchMode("roleAdmID", FetchMode.JOIN);
                cr.add(Restrictions.eq("username", userName));
                cr.add(Restrictions.eq("isDeleted", false));
                return (Admin) cr.uniqueResult();
            }
        } catch (HibernateException e) {
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return null;
    }
}
