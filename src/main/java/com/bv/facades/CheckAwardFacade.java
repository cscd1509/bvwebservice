package com.bv.facades;

import com.bv.entities.CheckAward;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

public class CheckAwardFacade extends AbstractFacade {

    public CheckAwardFacade() {
        super(CheckAward.class);
    }

    public List<CheckAward> getAvailableCheckAward() {
        List<CheckAward> rs = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            Criteria cr = session.createCriteria(CheckAward.class);
            cr.add(Restrictions.eq("isDeleted", false));
            cr.addOrder(Order.asc("name"));
            rs = cr.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
}
