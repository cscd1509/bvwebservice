package com.bv.facades;

import com.bean.CustomerTree;
import com.bv.entities.Customer;
import com.bv.utils.CustomFunction;
import java.util.List;
import javafx.util.Pair;
import org.hibernate.type.StringType;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;

public class CustomerFacade extends AbstractFacade {

    public CustomerFacade() {
        super(Customer.class);
    }

    public Pair<Integer, String> createUser(String fullName, String password, String email, String refIDUserName,
            String mobile, Boolean gender, String username, String peoplesIdentity, String bankName, String bankNumber, String bankAgency) throws Exception{
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("CreateUser :fullName,:password,:email,:refIDUserName,:mobile,:gender,:username"
                    + ",:peoplesIdentity,:bankName,:bankNumber,:bankAgency")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("fullName", fullName)
                    .setParameter("password", password)
                    .setParameter("email", email)
                    .setParameter("refIDUserName", refIDUserName)
                    .setParameter("mobile", mobile)
                    .setParameter("gender", gender)
                    .setParameter("username", username)
                    .setParameter("peoplesIdentity", peoplesIdentity)
                    .setParameter("bankName", bankName)
                    .setParameter("bankNumber", bankNumber)
                    .setParameter("bankAgency", bankAgency);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }    

    public List getBasicInfomation(String username) throws Exception{
        Session session = null;
        List result = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("GetInformationByUserName :username")
                        .addScalar("ID", IntegerType.INSTANCE)
                        .addScalar("Name", StringType.INSTANCE)
                        .addScalar("ValuesName", StringType.INSTANCE)
                        .setParameter("username", username);
                q.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                result = q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
    

    public List<CustomerTree> getTreeParent(String first_username, Integer max_level) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session == null) {
                return null;
            }
            Query q = session.createSQLQuery("API_TreeParent :first_username,:max_level").addEntity(CustomerTree.class);
            q.setParameter("first_username", first_username);
            q.setParameter("max_level", max_level);
            list = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }

    public List<CustomerTree> getTreeReferral(String first_username, Integer max_level) {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session == null) {
                return null;
            }
            Query q = session.createSQLQuery("API_TreeReferral :first_username,:max_level").addEntity(CustomerTree.class);
            q.setParameter("first_username", first_username);
            q.setParameter("max_level", max_level);
            list = q.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
