package com.bv.facades;

import com.bean.PagerPlus;
import com.bv.entities.HistoryAward;
import com.bv.transformer.HistoryAwardResultTransformer;
import com.bv.utils.StringUtils;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

public class HistoryAwardFacade extends AbstractFacade {

    public HistoryAwardFacade() {
        super(HistoryAward.class);
    }

    public List<HistoryAward> pager(PagerPlus pager, String userName) throws Exception {
        Session session = null;
        List<HistoryAward> rs = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Criteria cr = session.createCriteria(HistoryAward.class, "h");
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("partnerID", "partnerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("checkAwardID", "checkAwardID", JoinType.LEFT_OUTER_JOIN);
                Conjunction conj = Restrictions.conjunction();
                conj.add(Restrictions.eq("isDeleted", false));
                if (!StringUtils.isEmpty(userName)) {
                    conj.add(Restrictions.eq("customerID.userName", userName));
                }
                if (!StringUtils.isEmpty(pager.getKeyword())) {
                    conj.add(Restrictions.like("partnerID.userName", pager.getKeyword(), MatchMode.ANYWHERE));
                }
                if (pager.getIsCheck() != null) {
                    conj.add(Restrictions.eq("checkAwardID.id", pager.getIsCheck()));
                }
                if (pager.getIsAvailability() != null) {
                    switch (pager.getIsAvailability()) {
                        case 0: {
                            conj.add(Restrictions.eq("isAvailability", false));
                            break;
                        }
                        case 1: {
                            conj.add(Restrictions.eq("isAvailability", true));
                            break;
                        }
                    }
                }
                if (pager.getStartDate() != null || pager.getEndDate() != null) {
                    if (pager.getStartDate() == null) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, c.get(Calendar.YEAR) - 100);
                        pager.setStartDate(c.getTime());
                    }
                    if (pager.getEndDate() == null) {
                        Calendar c = Calendar.getInstance();
                        c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 100);
                        pager.setEndDate(c.getTime());
                    }
                    conj.add(Restrictions.between("createdDate", pager.getStartDate(), pager.getEndDate()));
                }
                cr.add(conj);
                cr.setProjection(Projections.sum("amount"));
                pager.setAmount((BigDecimal) cr.uniqueResult());
                cr.setProjection(Projections.rowCount());
                pager.setTotalResult(((Long) cr.uniqueResult()).intValue());
                cr = session.createCriteria(HistoryAward.class, "h");
                cr.createAlias("customerID", "customerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("partnerID", "partnerID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("checkAwardID", "checkAwardID", JoinType.LEFT_OUTER_JOIN);
                cr.createAlias("circleWithdrawID", "circleWithdrawID", JoinType.LEFT_OUTER_JOIN);
                cr.add(conj);
                cr.setFirstResult(pager.getFirstResult());
                cr.setMaxResults(pager.getDisplayPerPage());
                cr.addOrder(pager.getAsc() ? Order.asc(pager.getOrderColumn()) : Order.desc(pager.getOrderColumn()));
                cr.setProjection(
                        Projections.projectionList()
                                .add(Projections.property("id"))
                                .add(Projections.property("partnerID.id"))
                                .add(Projections.property("partnerID.fullName"))
                                .add(Projections.property("partnerID.userName"))
                                .add(Projections.property("checkAwardID.id"))
                                .add(Projections.property("checkAwardID.name"))
                                .add(Projections.property("circleWithdrawID.id"))
                                .add(Projections.property("circleWithdrawID.fromDate"))
                                .add(Projections.property("circleWithdrawID.toDate"))
                                .add(Projections.property("amount"))
                                .add(Projections.property("createdDate"))
                                .add(Projections.property("isAvailability"))
                );
                cr.setResultTransformer(new HistoryAwardResultTransformer());
                rs = cr.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return rs;
    }
}
