package com.bv.facades;

import com.bv.entities.HistoryChangeRank;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

public class HistoryChangeRankFacade extends AbstractFacade {

    public HistoryChangeRankFacade() {
        super(HistoryChangeRank.class);
    }

    public List pager(String userName) throws Exception {
        Session session = null;
        List list = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createQuery("SELECT new HistoryChangeRank(hcr.id,rankID.id,rankID.name,rankID.color,hcr.rankName,hcr.createdDate) FROM HistoryChangeRank AS hcr LEFT JOIN hcr.rankID AS rankID WHERE hcr.customerID.userName=:userName ORDER BY hcr.createdDate DESC");
                q.setParameter("userName", userName);
                list = q.list();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return list;
    }
}
