package com.bv.facades;

import com.bv.entities.HistoryDeposit;
import java.math.BigDecimal;
import javafx.util.Pair;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

public class HistoryDepositFacade extends AbstractFacade {

    public HistoryDepositFacade() {
        super(HistoryDeposit.class);
    }

    public Pair<Integer, String> deposit(int sendingAdminID, String receivingCustomerUserName, BigDecimal amount, BigDecimal discountPercent) throws Exception {
        Transaction trans = null;
        Session session = null;
        Pair<Integer, String> result = new Pair(0, "Đã xảy ra lỗi. Vui lòng thực hiện lại sau!");
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            Query q = session.createSQLQuery("TransferAdminToCustomer :sendingAdminID,:receivingCustomerUserName,:amount,:discountPercent")
                    .addScalar("Result", IntegerType.INSTANCE).addScalar("Msg", StringType.INSTANCE);
            q.setParameter("sendingAdminID", sendingAdminID)
                    .setParameter("receivingCustomerUserName", receivingCustomerUserName)
                    .setParameter("amount", amount)
                    .setParameter("discountPercent", discountPercent);
            Object[] row = (Object[]) q.uniqueResult();
            result = new Pair<>((Integer) row[0], (String) row[1]);
            trans.commit();
        } catch (HibernateException e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return result;
    }
}
