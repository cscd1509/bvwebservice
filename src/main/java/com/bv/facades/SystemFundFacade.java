package com.bv.facades;

import com.bv.entities.Admin;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.StringType;

public class SystemFundFacade extends AbstractFacade {

    public SystemFundFacade() {
        super(Admin.class);
    }

    public List showCompany() throws Exception {
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            if (session != null) {
                Query q = session.createSQLQuery("exec Show_Company")
                        .addScalar("Name", StringType.INSTANCE)
                        .addScalar("ValueNow", BigDecimalType.INSTANCE);
                return q.list();
            }
        } catch (HibernateException e) {
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
        return null;
    }
}
