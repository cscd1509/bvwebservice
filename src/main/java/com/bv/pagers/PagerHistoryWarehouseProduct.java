package com.bv.pagers;

public class PagerHistoryWarehouseProduct extends Pager {

    private Integer salaryCycleID;
    private Integer wareHouseID;
    private Integer historyTypeID;

    public PagerHistoryWarehouseProduct() {
        super();
    }

    public Integer getSalaryCycleID() {
        return salaryCycleID;
    }

    public void setSalaryCycleID(Integer salaryCycleID) {
        this.salaryCycleID = salaryCycleID;
    }

    public Integer getWareHouseID() {
        return wareHouseID;
    }

    public void setWareHouseID(Integer wareHouseID) {
        this.wareHouseID = wareHouseID;
    }

    public Integer getHistoryTypeID() {
        return historyTypeID;
    }

    public void setHistoryTypeID(Integer historyTypeID) {
        this.historyTypeID = historyTypeID;
    }
}
