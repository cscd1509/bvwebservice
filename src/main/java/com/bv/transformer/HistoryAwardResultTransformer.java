/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bv.transformer;

import com.bv.entities.CheckAward;
import com.bv.entities.CircleWithdraw;
import com.bv.entities.Customer;
import com.bv.entities.HistoryAward;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.transform.ResultTransformer;

/**
 *
 * @author pc3-cellx
 */
public class HistoryAwardResultTransformer implements ResultTransformer {

    @Override
    public Object transformTuple(Object[] os, String[] strings) {
        HistoryAward ha = new HistoryAward();
        ha.setId((int) os[0]);
        try {
            Customer partnerID = new Customer();
            partnerID.setId((int) os[1]);
            partnerID.setFullName((String) os[2]);
            partnerID.setUserName((String) os[3]);
            ha.setPartnerID(partnerID);
        } catch (Exception e) {
        }
        try {
            CheckAward checkAwardID = new CheckAward();
            checkAwardID.setId((int) os[4]);
            checkAwardID.setName((String) os[5]);
            ha.setCheckAwardID(checkAwardID);
        } catch (Exception e) {
        }
        try {
            CircleWithdraw circleWithdrawID = new CircleWithdraw();
            circleWithdrawID.setId((int) os[6]);
            circleWithdrawID.setFromDate((Date) os[7]);
            circleWithdrawID.setToDate((Date) os[8]);
            ha.setCircleWithdrawID(circleWithdrawID);
        } catch (Exception e) {
        }
        ha.setAmount((BigDecimal) os[9]);
        ha.setCreatedDate((Date) os[10]);
        ha.setIsAvailability((boolean) os[11]);
        return ha;
    }

    @Override
    public List transformList(List list) {
        return list;
    }
}
