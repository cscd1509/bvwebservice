package com.bv.utils;

import com.bv.entities.Action;
import com.bv.entities.Admin;
import com.bv.entities.AdminLogs;
import com.bv.entities.Module;
import com.bv.facades.HibernateConfiguration;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class LogUtils {

    public final static int ACTION_ADD = 1;
    public final static int ACTION_UPDATE = 2;
    public final static int ACTION_DELETE = 3;
    public final static int ACTION_NAPTIEN = 4;
    public final static int ACTION_YEUCAU_NAPTIEN = 5;
    public final static int ACTION_DUYET = 6;

    public static void logs(int adminID, int actionID, int moduleID, String content) {
        Transaction trans = null;
        Session session = null;
        try {
            session = HibernateConfiguration.getInstance().openSession();
            trans = session.beginTransaction();
            AdminLogs log = new AdminLogs();
            log.setAdmin(session.get(Admin.class, adminID));
            log.setAction(session.get(Action.class, actionID));
            log.setModule(session.get(Module.class, moduleID));
            log.setContent(content);
            session.save(log);
            trans.commit();
        } catch (Exception e) {
            try {
                if (trans != null) {
                    trans.rollback();
                }
            } catch (Exception ex) {
                throw ex;
            }
            e.printStackTrace();
            throw e;
        } finally {
            HibernateConfiguration.getInstance().closeSession(session);
        }
    }

}
