package com.exception;

/**
 *
 * @author pc3-cellx
 */
public class ExistedCheckInTimeException extends Exception {

    public ExistedCheckInTimeException(String messString) {
        super(messString);
    }
}
